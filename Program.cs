﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Tesseract;

namespace OCR
{
    class Program
    {
        [DllImport("User32.dll")]
        static extern IntPtr GetDC(IntPtr hwnd);

        public static void Draw(Rectangle r, Brush b, IntPtr hwnd)
        {
            using (var g = Graphics.FromHdc(hwnd))
            {
                g.FillRectangle(b, r);
            }
        }

        public static Bitmap PrintScreen(int w, int h, int x = 0, int y = 0)
        {
            using (var bitmap = new Bitmap(w, h))
            {
                using (var g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(new Point(x, y), Point.Empty, Screen.PrimaryScreen.Bounds.Size);
                    Draw(new Rectangle(x, y, w, h), Brushes.Red, GetDC(IntPtr.Zero));
                }
                return bitmap;
            }
        }

        static void Main(string[] args)
        {
            var testImage = PrintScreen(150, 30);
            try
            {
                using (var engine = new TesseractEngine(@"tessdata", "eng", EngineMode.Default))
                {
                    using (var img = PixConverter.ToPix(testImage))
                    {
                        using (var page = engine.Process(img))
                        {
                            var texto = page.GetText();
                            Console.WriteLine($"Taxa de precisao {0}", page.GetMeanConfidence());
                            Console.WriteLine($"Texto: {0}", texto);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Erro: {0}", ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}
